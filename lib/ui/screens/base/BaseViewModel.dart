import 'package:first_sample_app/data/DataManagerService.dart';
import 'package:first_sample_app/data/api/network_exceptions/NetworkExceptions.dart';
import 'package:first_sample_app/data/prefs/SharedpreferenceService.dart';
import 'package:first_sample_app/di/locator.dart';
import 'package:first_sample_app/utils/enums/BottomSheetTypes.dart';
import 'package:first_sample_app/utils/enums/ViewState.dart';
import 'package:flutter/material.dart';
import 'package:stacked_services/stacked_services.dart';

class BaseModel extends ChangeNotifier {
  DataManagerService _appDataManager = locator<DataManagerService>();
  BottomSheetService _bottomSheetService = locator<BottomSheetService>();
  SharedPreferenceService _sharedPreferenceService =
      locator<SharedPreferenceService>();
  NavigationService _navigationService = locator<NavigationService>();

  ViewState _state = ViewState.Idle;

  ViewState get state => _state;

  void setState(ViewState viewState) {
    _state = viewState;
    notifyListeners();
  }

  DataManagerService getDataManager() => _appDataManager;

  SharedPreferenceService getSharedPreferenceService() =>
      _sharedPreferenceService;

  NavigationService getNavigationService() => _navigationService;

  BottomSheetService getBottomSheetService() => _bottomSheetService;

  Future<bool> handleApiError(
    NetworkExceptions error,
    Function callback, [
    bool isDismissible = false,
    String mainButtonTitle = "RETRY",
  ]) async {
    if (error == NetworkExceptions.noInternetConnection()) {
      await showNoInternetAlert(
          title: "No Internet Connection",
          description: "Please make sure that you are connected to internet",
          isDismissible: isDismissible,
          mainButtonTitle: mainButtonTitle);
      callback();
      return true;
    } else {
      return false;
    }
  }

  Future handleGeneralApiError(NetworkExceptions error, Function callback,
      {String title = "Problem occurred",
      String description = "Some problem occurred. Please try again!",
      bool isDismissible = true,
      String mainButtonTitle = "RETRY"}) async {
    bool handled = await handleApiError(error, () async {
      callback();
    }, isDismissible, mainButtonTitle);

    if (!handled) {
      showErrorDialog(
        title: title,
        description: description,
        isDismissible: isDismissible,
      );
    }
  }

  showErrorDialog(
      {String title = "Problem occured",
      String description = "Some problem occured Please try again",
      bool isDismissible = true}) async {
    await _bottomSheetService.showCustomSheet(
      title: title,
      description: description,
      mainButtonTitle: "OK",
      variant: BottomSheetTypes.error,
      barrierDismissible: isDismissible,
    );
  }

  showNoInternetAlert(
      {String title = "PhoneAuthHolderViewModelnet Connection",
      String description = "Please Connect to Stable Network and Try Again",
      bool isDismissible = false,
      mainButtonTitle = "RETRY"}) async {
    await _bottomSheetService.showCustomSheet(
      title: title,
      description: description,
      mainButtonTitle: mainButtonTitle,
      variant: BottomSheetTypes.noInternet,
      barrierDismissible: isDismissible,
    );
  }

  runStartupLogic() async {}
}
