import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

class FirebaseAuthService {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final GoogleSignIn _googleSignIn = GoogleSignIn();

  Future<String> signInWithGoogleAndStoreToken() async {
    try {
      final GoogleSignInAccount? googleSignInAccount =
          await _googleSignIn.signIn();

      if (googleSignInAccount != null) {
        final GoogleSignInAuthentication googleSignInAuthentication =
            await googleSignInAccount.authentication;

        final AuthCredential credential = GoogleAuthProvider.credential(
          accessToken: googleSignInAuthentication.accessToken,
          idToken: googleSignInAuthentication.idToken,
        );

        await _auth.signInWithCredential(credential);
        return "Success";
      } else {
        return "account not selected";
      }
    } catch (e) {
      return e.toString();
    }
  }

  Future<String> signInWithOTP(smsCode, verId) async {
    AuthCredential authCredentials = PhoneAuthProvider.credential(
      verificationId: verId,
      smsCode: smsCode,
    );
    String status = await signIn(authCredentials);
    return status;
  }

  Future<String> signIn(AuthCredential authCredentials) async {
    String returnString = "error";
    try {
      UserCredential authResponse =
          await FirebaseAuth.instance.signInWithCredential(authCredentials);

      User? _user = authResponse.user;
      if (_user != null && _user.phoneNumber != null) {
        returnString = "no-error";
      } else {
        returnString = "error";
      }
      return returnString;
    } on FirebaseAuthException catch (e) {
      return e.message.toString();
    }
  }

  Future<bool> signOut() async {
    await _googleSignIn.signOut();
    await _auth.signOut();
    return true;
  }
}
