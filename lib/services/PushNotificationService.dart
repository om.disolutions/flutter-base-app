
import 'package:firebase_messaging/firebase_messaging.dart';

class PushNotificationService {
  Future<String> getFcmToken() async {
    FirebaseMessaging _fcmInstance = FirebaseMessaging.instance;
    String? fcmToken = await _fcmInstance.getToken();
    return fcmToken.toString();
  }
}