import 'package:first_sample_app/data/DataManagerService.dart';
import 'package:first_sample_app/data/api/ApiService.dart';
import 'package:first_sample_app/data/local/hive_db/HiveDbService.dart';
import 'package:first_sample_app/data/prefs/SharedpreferenceService.dart';
import 'package:first_sample_app/services/FirebaseAuthService.dart';
import 'package:first_sample_app/ui/screens/base/BaseViewModel.dart';
import 'package:get_it/get_it.dart';
import 'package:stacked_services/stacked_services.dart';

GetIt locator = GetIt.I;

void setupLocator() {
  //singleton => will return the same object
  //factory => will return new object everytime
  locator.registerLazySingleton(() => BottomSheetService());
  locator.registerLazySingleton(() => DialogService());
  locator.registerLazySingleton(() => NavigationService());
  locator.registerFactory(() => BaseModel());
  locator.registerLazySingleton(() => DataManagerService());
  locator.registerLazySingleton(() => FirebaseAuthService());
  locator.registerLazySingleton(() => SharedPreferenceService());
  locator.registerLazySingleton(() => ApiService());
  locator.registerLazySingleton(() => HiveDbService());

}