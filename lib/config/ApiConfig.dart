import 'package:flutter_dotenv/flutter_dotenv.dart';

class ApiConfig{
  //API Related constants will be declared here
  static final String baseUrl = "";
  static final String apiKey = dotenv.env['APIKEY'] ?? "";
}