class DimensionConstants {
  //All Dimensions will be declared here
  static const double top_padding_for_swipe_card = 34;
  static const double bottom_padding_for_swipe_card = 100;
  static const double device_height_constraint = 700;
  static const double device_width_constraint = 600;
  static const double larger_screens_constant = 2;
  static const double medium_screens_constant = 0.7;
}
