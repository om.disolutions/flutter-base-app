import 'package:first_sample_app/config/ColorConstants.dart';
import './SizeConfig.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

TextStyle pageTitleTextStyle = TextStyle(
  fontSize: 18.horizontal(),
  fontWeight: FontWeight.w700,
  color: ColorConstants.greyColorBackground,
);

BoxDecoration editProfileYellowContainerBoxDecoration = BoxDecoration(
  color: ColorConstants.greyColorBackground,
  borderRadius: BorderRadius.circular(5.horizontal()),
  boxShadow: [
    BoxShadow(
      offset: Offset(0, 0),
      color: ColorConstants.greyColorBackground,
      blurRadius: 15.vertical(),
    ),
  ],
);
