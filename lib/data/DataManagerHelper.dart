import 'api/ApiHelper.dart';
import 'prefs/SharedPreferenceHelper.dart';
import './local/hive_db/HiveDbHelper.dart';

abstract class DataManagerHelper
    implements ApiHelper, SharedPreferenceHelper, HiveDbHelper {}
