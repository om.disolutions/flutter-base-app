import "package:dio/dio.dart";
import 'package:first_sample_app/config/ApiConfig.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

class Client {
  String baseUrl = ApiConfig.baseUrl;
  String apiKey = ApiConfig.apiKey;
  Dio? _dio;
  BaseOptions options = new BaseOptions(
    connectTimeout: 10000,
    receiveTimeout: 5000,
  );

  String? token;
  Map<String, Object>? header;

  Client({this.token});

  Client builder() {
    header = Map<String, Object>();
    header!.putIfAbsent('Accept', () => 'application/json');
    header!.putIfAbsent('x-api-key', () => apiKey);
    header!.putIfAbsent('Content-Type', () => 'application/json');
    _dio = new Dio(options);
    _dio!.interceptors.add(_interceptorsWrapper);
    _dio!.interceptors.add(PrettyDioLogger());
    _dio!.options.baseUrl = baseUrl;
    _dio!.options.headers = header;
    return this;
  }

  Client acceptHtml() {
    header!.remove('Accept');
    return this;
  }

  Client setUrlEncoded() {
    header!.remove('Content-Type');
    header!
        .putIfAbsent('Content-Type', () => 'application/x-www-form-urlencoded');
    _dio!.options.headers = header;
    return this;
  }

  Client removeContentType() {
    header!.remove('Content-Type');
    return this;
  }

  Client removeAndAddAccept() {
    header!.remove('Accept');
    header!.putIfAbsent("Accept", () => "*/*");
    return this;
  }

  Client setMultipartFormDataHeader() {
    header!.remove('Content-Type');
    header!.putIfAbsent('Content-Type', () => 'multipart/form-data');
    _dio!.options.headers = header;
    return this;
  }

  Future<Client> setProtectedApiHeader() async {
    // DataManager _dataManager = locator<DataManager>();
    // String? token = await _dataManager.getServerAuthToken();
    header!.putIfAbsent('Authorization', () => 'Bearer $token');
    return this;
  }

  Dio build() {
    return _dio!;
  }
}

InterceptorsWrapper _interceptorsWrapper = InterceptorsWrapper(
  onRequest: (options, handler) {
    return handler.next(options); //continue
  },
  onResponse: (response, handler) {
    return handler.next(response); // continue
  },
  onError: (DioError e, handler) {
    return handler.next(e); //continue
  },
);
